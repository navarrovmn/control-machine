#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <zmq.h>
#include <unistd.h>

#define esteira 7
#define sensorFluxo 21
#define sensorNivel 3
#define emergencia 1
#define liga 27
#define bomba 29
#define sensorEncher 22
#define sensorFechar 5

#define pistao 0
#define sensorIndutivo 24
#define sensorPosicaoTampa 4

// Flags

int ehMesmaGarrafa1 = 0;
int ehMesmaGarrafa2 = 0;
int ehMesmaGarrafa3 = 0;
int jaVerificouTampa = 0;

// Flags de software

int iniciouCiclo = 0;
int fechouGarrafa = 0;
int naoTemCerveja = 0;
int naoTemTampa = 0;
int botaoEmergencia = 0;
int encheuGarrafa = 0;
void *context;
void *requester;

// Interacoes com sensores

int botaoLigadoPressionado(){
	return digitalRead(liga);
}

int botaoEmergenciaPressionado(){
	return digitalRead(1);
}

void ligaEsteira(){
	digitalWrite(esteira, HIGH);
	delay(10);
}

void desligaEsteira(){
	digitalWrite(esteira, LOW);
	delay(10);
}


int verificaSensorPosicao1(){
	return digitalRead(sensorEncher);
}

int verificaSensorPosicao2(){
	return digitalRead(sensorPosicaoTampa);
}

int verificaSensorPosicao3(){
	return digitalRead(sensorFechar);
}

int garrafaComTampa(){
	return !digitalRead(sensorIndutivo);
}

void ligaBomba(){
	digitalWrite(bomba, HIGH);
	delay(250);
}

void desligaBomba(){
	digitalWrite(bomba, LOW);
	delay(250);
}

void ligaPistao(){
	digitalWrite(pistao, HIGH);
}

void desligaPistao(){
	digitalWrite(pistao, LOW);
}

// Funcoes de acao

void envasar(){
	if(verificaSensorPosicao1() && !ehMesmaGarrafa1){
		ehMesmaGarrafa1 = 1;
		desligaEsteira();
		sleep(4);
		ligaBomba();
		printf("Enchendo...\n");
		sleep(14);
		desligaBomba();
		if(!encheuGarrafa){
			encheuGarrafa = 1;
			zmq_send(requester, "6", 1, 0);
		}
	}else if(!verificaSensorPosicao1()){
		ehMesmaGarrafa1 = 0;
		encheuGarrafa = 0;
	}
}

void fechaGarrafa(){
	if(verificaSensorPosicao3() && !ehMesmaGarrafa3 && !digitalRead(sensorNivel)){	
		ehMesmaGarrafa3 = 1;
		delay(125);
		desligaEsteira();
		sleep(4);
		printf("Pistao ligado\n");
		ligaPistao();
		printf("Fechando...\n");
		sleep(6);
		printf("Pistao desligado\n");
		desligaPistao();
		sleep(2);
		if(!fechouGarrafa){
			fechouGarrafa = 1;
			zmq_send(requester, "2", 1, 0);
		}
	}else if(!verificaSensorPosicao3()){
		ehMesmaGarrafa3 = 0;
		fechouGarrafa = 0;
	}
}

void verificaTampa(){
	if(verificaSensorPosicao2() && !ehMesmaGarrafa2){
		if(!garrafaComTampa()){	
			zmq_send(requester, "4", 1, 0);
		}
		while(!garrafaComTampa()){
		 	desligaEsteira();
		 	if(!verificaSensorPosicao2()){
				break;
			}
		}
		 
		 ehMesmaGarrafa2 = 1;
	}else if(!verificaSensorPosicao2()){
		ehMesmaGarrafa2 = 0;
	}
}

int main(){
	
	wiringPiSetup();

	context = zmq_ctx_new();
	requester = zmq_socket(context, ZMQ_DEALER);
	zmq_connect(requester, "tcp://localhost:5588");

	pinMode(esteira, OUTPUT );
	pinMode(bomba, OUTPUT);
	pinMode(sensorEncher, INPUT);
	pinMode(pistao, OUTPUT);
	pinMode(sensorPosicaoTampa, INPUT);
	pinMode(sensorIndutivo, INPUT);
	pinMode(sensorFechar, INPUT);
	
	
	while(1){
		if(botaoLigadoPressionado() && !botaoEmergenciaPressionado() && !digitalRead(sensorNivel)){
			printf("Funcionando...\n");
			botaoEmergencia = 0;
			naoTemCerveja = 0;
			envasar();
			verificaTampa();
			fechaGarrafa();
			ligaEsteira();
			if(!iniciouCiclo){
				iniciouCiclo = 1;
				zmq_send(requester, "0", 1, 0);
			}
		}else if(botaoEmergenciaPressionado()){
			printf("Botao de emergencia pressionado\n");
			desligaEsteira();
			if(!botaoEmergencia){
				botaoEmergencia = 1;
				zmq_send(requester, "5", 1, 0);
			}
		}else if(digitalRead(sensorNivel)){
			printf("Nao ha cerveja\n");
			desligaEsteira();
			if(!naoTemCerveja){
				naoTemCerveja = 1;
				zmq_send(requester, "3", 1, 0);
			}
		}else{
			printf("Nem ta ligado\n");
			desligaEsteira();
			if(iniciouCiclo){
				iniciouCiclo = 0;
				zmq_send(requester, "1", 1, 0);
			}
		}
	}
	return 0;
}
